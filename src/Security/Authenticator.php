<?php

namespace App\Security;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class Authenticator extends AbstractGuardAuthenticator
{
    /**
     * @var SessionInterface
     */
    public $session;

    /**
     * @var UrlGeneratorInterface
     */
    public $urlGenerator;

    public function __construct(SessionInterface $sessionInterface, UrlGeneratorInterface $urlGeneratorInterface)
    {
        $this->session = $sessionInterface;
        $this->urlGenerator = $urlGeneratorInterface;
    }
    public function supports(Request $request)
    {
        $login = $this->session->get('login');
        return $login == true;
    }

    public function getCredentials(Request $request)
    {
        return $request;
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        return new User('admin','password', ['ROLE_USER']);
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        // todo
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $providerKey)
    {
        // todo
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        return new RedirectResponse($this->urlGenerator->generate('admin.login'));
    }

    public function supportsRememberMe()
    {
        // todo
    }
}
