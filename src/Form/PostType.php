<?php

namespace App\Form;

use App\Entity\Post;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'required' => true,
                'error_bubbling'=> true,
                'attr' => [
                    'placeholder'=> 'title',
                    'maxlength' => 160
                ],
                'constraints' => [
                    new Length([
                        'max' => 160,
                        'maxMessage' => 'The maximum title length is {{ limit }} characters'
                    ])
                ]
            ])
            ->add('excerpt', TextareaType::class, [
                'required' => true,
                'error_bubbling'=> true,
                'attr' => [
                    'placeholder' => 'excerpt',
                    'maxlength' => 160,

                    'resizable' => false
                ],
                'constraints' => [
                    new Length([
                        'max' => 160,
                        'maxMessage' => 'The maximum excerpt length is {{ limit }} characters'
                    ])
                ]
            ])
            ->add('body', TextareaType::class, [
                'required' => true,
                'error_bubbling'=> true,
                'attr' => [
                    'class' => 'ckeditor'
                ],
            ])
            ->add('public', CheckboxType::class, [
                'error_bubbling'=> true,
                'required' => false,
            ])
            ->add('submit', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Post::class,
            
        ]);
    }
}
