<?php

namespace App\Controller;

use App\Entity\Post;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class MainController extends AbstractController
{

    /**
     * @var EntityManagerInterface
     */
    public $em;


    public function __construct(EntityManagerInterface $entityManagerInterface)
    {
        $this->em = $entityManagerInterface;
    }
    // home page route
    public function index()
    {

        $posts = $this->em->getRepository(Post::class)->getHome();

        $posts = array_map(function (Post $post) {
            return [
                'id' => $post->getId(),
                'title' => $post->getTitle(),
                'excerpt' => $post->getExcerpt(),
                'slug' => $post->getSlug()
            ];
        }, $posts);

        return $this->render('index.html.twig', [
            'posts' => $posts
        ]);
    }

    // archive page route
    public function archive($page)
    {
        $pageSize = 10;


        $posts = $this->em->getRepository(Post::class)->getArchivePage($page, $pageSize);


        $numPages = ceil($this->em->getRepository(Post::class)->getCount() / $pageSize);

        if (count($posts) == 0) {
            return $this->redirectToRoute('index');
        } else {
            $posts = array_map(function (Post $post) {
                return [
                    'id' => $post->getId(),
                    'title' => $post->getTitle(),
                    'excerpt' => $post->getExcerpt(),
                    'slug' => $post->getSlug()
                ];
            }, $posts);
            return $this->render('archive.html.twig', [
                'posts' => $posts,
                'pages' => $numPages
            ]);
        }
    }

    // sitemap post route
    public function post($slug)
    {
        /**
         * @var Post
         */
        $post =  $this->em->getRepository(Post::class)->findOneBy([
            'slug' => $slug
        ]);
        if ($post == null) {
            return $this->redirectToRoute('index');
        }

        $post = [
            'title' => $post->getTitle(),
            'excerpt' => $post->getExcerpt(),
            'body' => $post->getBody(),
            'created' => $post->getCreated()
        ];
        return $this->render('post.html.twig', [
            'post' => $post
        ]);
    }

    // sitemap route
    public function sitemap()
    {
        $content = '<?xml version="1.0" encoding="UTF-8"?>
        <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
        <url>
        <loc>' . $this->generateUrl('index', [], UrlGeneratorInterface::ABSOLUTE_URL) . '</loc>
        <lastmod>' . date('Y-m-d') . '</lastmod>
        <priority>1.0</priority>
        </url>';

        $numPages = ceil($this->em->getRepository(Post::class)->getCount() / 10);
        for ($i = 0; $i < $numPages; $i++) {
            $content .= '<url>
            <loc>' . $this->generateUrl('archive', ['page' => $i + 1], UrlGeneratorInterface::ABSOLUTE_URL) . '</loc>
            <lastmod>' . date('Y-m-d') . '</lastmod>
            <priority>0.8</priority>
            </url>';
        }

        $posts = $this->em->getRepository(Post::class)->findAll();

        foreach ($posts as $key => $post) {
            $content .= '<url>
            <loc>' . $this->generateUrl('post', ['slug' => $post->getSlug()], UrlGeneratorInterface::ABSOLUTE_URL) . '</loc>
            <lastmod>' . date('Y-m-d') . '</lastmod>
            <priority>0.6</priority>
            </url>';
        }



        $content .= '</urlset>';

        return new Response($content, 200, [
            'Content-type' => 'application/xml; charset=utf-8'
        ]);
    }
}
