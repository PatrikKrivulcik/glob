<?php


namespace App\Controller;

use App\Entity\Post;
use App\Form\PostType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\String\Slugger\SluggerInterface;

class AdminController extends AbstractController
{
    /**
     * @var SessionInterface
     */
    public $session;

    /**
     * @var EntityManagerInterface
     */
    public $em;

    /**
     * @var SluggerInterface
     */
    public $slugger;

    public function __construct(SessionInterface $session, EntityManagerInterface $em, SluggerInterface $sluggerInterface)
    {
        $this->session = $session;
        $this->em = $em;
        $this->slugger = $sluggerInterface;
    }


    // login route

    public function login(Request $request)
    {
        if ($this->session->get('login') == true) {
            return $this->redirectToRoute('admin.all');
        }
        if ($request->getMethod() == 'POST') {
            $username = $request->get('username');
            $password = $request->get('password');

            if ($username == $_ENV['ADMIN_USERNAME'] && $password == $_ENV['ADMIN_PASSWORD']) {
                $this->session->set('login', true);
                return $this->redirectToRoute('admin.all');
            } else {
                $this->session->set('login', false);
            }
        }
        return $this->render('login.html.twig');
    }

    // logout route

    public function logout()
    {
        $this->session->set('login', false);
        return $this->redirectToRoute('index');
    }



    // all route
    public function all()
    {
        $all = $this->em->getRepository(Post::class)->findAll();
        $all = array_map(function (Post $post) {
            return [
                'id'  => $post->getId(),
                'title' => $post->getTitle(),
                'public' => $post->getPublic(),
                'created' => $post->getCreated()
            ];
        }, $all);

        return $this->render('admin/all.html.twig', [
            'all' => array_reverse($all)
        ]);
    }

    // edit route
    public function edit($id, Request $request)
    {

        $post = $this->em->getRepository(Post::class)->find($id);


        $form = $this->createForm(PostType::class, $post);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $post = $form->getData();
            $post->setSlug(
                strtolower(
                    $this->slugger->slug($post->getTitle())
                )
            );

            $this->em->persist($post);
            $this->em->flush();

            return $this->redirectToRoute('admin.all');
        }


        return $this->render('admin/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }

    // add route
    public function add(Request $request)
    {


        $form = $this->createForm(PostType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $post = $form->getData();
            $post->setSlug(
                strtolower(
                    $this->slugger->slug($post->getTitle())
                )
            );

            $this->em->persist($post);
            $this->em->flush();

            return $this->redirectToRoute('admin.all');
        }


        return $this->render('admin/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
